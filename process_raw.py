import sys, os, fnmatch
import pandas as pd
from argparse import ArgumentParser
from converter import create_full_df

def find_file(pattern, path):
    result = []
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))
    return result

def main(indir):

    hdfname = indir + "/dataframe.h5"
    if os.path.exists(hdfname):
        print("Output file already exists!")
        #exit(0)

    print("Searching for files in " + indir)
    fnames = find_file('fifo4.raw', indir)#[:160]
    #fnames = glob.glob(indir + "/*/*/fifo4.raw")
    print("Found %i files" %len(fnames))

    dfs = []

    cnt = 0
    for i, fname in enumerate(sorted(fnames)):

        if i%10==0: print("File %i" %i)

        #print fname
        if os.path.getsize(fname) < 100:
            print os.path.getsize(fname)
            continue

        fname1 = fname
        fname2 = fname.replace("fifo4","fifo5")

        df_chans = create_full_df(fname1, fname2)
        if df_chans is None: continue

        df_chans["df_id"] = cnt

        # decode parameters
        dname = os.path.dirname(fname)
        param_strings = dname[dname.find("run_"):].split("/")[1:]

        params = {}
        for param_string in param_strings:
            #print param_string
            parts = param_string.split("_")

            if len(parts) % 2 == 0:
                for i in range(0, len(parts), 2):
                    param = parts[i]
                    value = parts[i+1]

                    # set value
                    try:
                        value = int(value)
                    except ValueError:
                        pass

                    params[param] = value
                    df_chans[param] = value

        if len(params) > 0:
            print "Params:", params
        #continue
        cnt+=1
        df_chans.reset_index(inplace = True)

        dfs.append(df_chans)

    print
    print("Used %i files" %cnt)

    # merger dfs
    df_chans = pd.concat(dfs)

    # downcast all columns to int32
    #for col in df_chans.columns:
    #    df_chans[col] = df_chans[col].astype("int32")

    print("Saving dataframe as " + hdfname)
    df_chans.to_hdf(hdfname, key = "data", complevel = 9)

    return

if __name__ == "__main__":

    parser = ArgumentParser()
    # parser arguments
    parser.add_argument("input_dir", type=str, default="./",
                        help="Input directory")

    args = parser.parse_args()
    main(args.input_dir)
