import sys, os, fnmatch
import pandas as pd
from argparse import ArgumentParser
from decode_trigger import get_df_trigger

def find_file(pattern, path):
    result = []
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))
    return result

def main(indir = "./", sumTC9 = 0):

    print("## Input directory:")
    print(indir)

    hdf_suffix = "_sumTC4"
    if sumTC9:
        print(36*"-")
        print("## Going to use 9 cell TC sums! ##")
        print(36*"-")
        hdf_suffix = "_sumTC9"

    hdfname = indir + "/trg_df%s.h5" %hdf_suffix
    if os.path.exists(hdfname):
        print("## Output file already exists!")

    print("## Searching for files in " + indir)
    fnames = find_file('fifo0.raw', indir)
    print("## Found %i files" %len(fnames))

    dfs = []

    cnt = 0
    for i, fname in enumerate(sorted(fnames)):
        indir = os.path.dirname(fname)

        if i%10==0: print("File %i" %i)

        #print fname
        if os.path.getsize(fname) < 2:
            print("Small file")
            print os.path.getsize(fname)
            continue

        df_chans = get_df_trigger(indir, sumTC9 = 0)

        # decode parameters
        dname = os.path.dirname(fname)
        param_strings = dname[dname.find("run_"):].split("/")[1:]

        params = {}
        for param_string in param_strings:
            parts = param_string.split("_")

            if len(parts) % 2 == 0:
                for i in range(0, len(parts), 2):
                    param = parts[i]
                    value = parts[i+1]

                    # cast value to int
                    try:
                        value = int(value)
                    except ValueError:
                        pass

                    params[param] = value
                    df_chans[param] = value

        if len(params) > 0:
            print "Params:", params
        #continue
        cnt+=1

        dfs.append(df_chans)

    print
    print("Used %i files" %cnt)

    # merge dfs
    df_chans = pd.concat(dfs)

    print("Saving dataframe as " + hdfname)
    df_chans.to_hdf(hdfname, key = "data", complevel = 9)

    return

if __name__ == "__main__":

    parser = ArgumentParser()
    # parser arguments
    parser.add_argument("input_dir", type=str, default="./",
                        help="Input directory")
    parser.add_argument("--sumTC9", action = "store_true",
                        help="Use 9 cell TC sums")

    args = parser.parse_args()
    main(args.input_dir, args.sumTC9)
