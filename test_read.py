from common import *

import matplotlib as mpl
mpl.use('agg')
import matplotlib.pyplot as plt

font = {'size'   : 18}
mpl.rc('font', **font)

if __name__ == "__main__":

    if len(sys.argv) == 2:
        indir = sys.argv[1]

        fname1 = indir + "/fifo4.raw"
        fname2 = indir + "/fifo5.raw"

        if not os.path.exists(fname1):
            print("Fifo4 file does not exist!")
            exit(0)
        if not os.path.exists(fname2):
            print("Fifo5 file does not exist!")
            exit(0)

        df_chan = create_full_df(fname1, fname2)

        outdir = indir + "/plots/"
        if not os.path.exists(outdir):
            os.makedirs(outdir)
        #make_plots(df_chan, outdir)
        make_plots(df_chan, outdir, 0)

    else:
        print("No argument given")
