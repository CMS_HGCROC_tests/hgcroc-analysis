import os, sys
import numpy as np
import pandas as pd

roc_half_shift = 36

def encode_tc_val(value, sumTC9 = 0):
    value = value.astype(int)

    log = np.log(value)/np.log(2)
    exp = (log).astype(int) - 3
    mant = (value >> (exp)) & ~(1<<3)
    return (8+mant) * np.power(2, (exp))

def decode_tc_val(value, sumTC9 = 0):

    mant = value & 0x7
    exp = (value >> 3) & 0xf

    exp += 2 * sumTC9

    return (8+mant) * np.power(2, (exp))

def get_df_trigger_link(fname, trg_link = 0, sumTC9 = 0):

    dfs = []

    with open(fname, 'r') as f:
        lines = f.read().splitlines()

    df_all = pd.DataFrame({"hex":lines})
    df_tc = []#{}

    tc_ch_offset = 4 * (trg_link//2 if sumTC9 else trg_link)

    for i in range(4):
        uncompressed_vals = df_all["hex"].apply(lambda x: decode_tc_val(int(x,16) >> 7*(3-i), sumTC9))
        df = uncompressed_vals.to_frame(name = "tc_sum")
        df.index.name = "event"
        df["tc"] = i + tc_ch_offset
        df_tc.append(df)

    df = pd.concat(df_tc)
    #df["bx"] = df.index % 10
    dfs.append(df)

    return pd.concat(dfs)

def get_df_trigger(indir, sumTC9 = 0):

    dfs = []
    trg_links = [0,2] if sumTC9 else range(4)

    for trg_link in trg_links:
        fname = indir + "/fifo%i.raw" %trg_link

        if not os.path.exists(fname):
            print("Warning! %s does not exist." %s)
            continue

        df = get_df_trigger_link(fname, trg_link, sumTC9)
        dfs.append(df)

    # merge trg links dfs
    df_all = pd.concat(dfs)

    return df_all

if __name__ == "__main__":

    if len(sys.argv) == 2:
        indir = sys.argv[1]

        #main(indir)
        df = get_df_trigger(indir, sumTC9 = 0)
        print df

    else:
        print("No argument given")
