from common import *

import matplotlib as mpl
mpl.use('agg')
import matplotlib.pyplot as plt

from scipy.stats import iqr

font = {'size'   : 18}
mpl.rc('font', **font)

def make_plots(dfs, labels, outdir = "./", plot_style = 0):
    print("Making plots into dir: " + outdir)
    # ## Plot
    title = outdir.split("/")
    title = title[-4]

    max_ch = 78#df_chan.channel.max()

    print("Mean and rms")
    # ## Mean and RMS of ADC, TOT, TOA
    for var in ["adc", "tot","toa"]:
        if plot_style == 0:
            f, axs = plt.subplots(2,1,figsize = (16,8), sharex = True, gridspec_kw={'height_ratios': [2, 1]})
            plt.subplots_adjust(wspace=0, hspace=0)
        else:
            f, axs = plt.subplots(1,2,figsize = (16,8))

        plt.suptitle(title)

        for i,df_chan in enumerate(dfs):
            label = labels[i]
            print label

            ax = axs[0]
            ax.set_ylabel("Mean " + var.upper())

            df_chan.groupby("channel")[var].mean().plot(ax = ax, marker = "o", linestyle = "--", label = label)


            if max_ch > roc_half_shift:
                ax.axvline(roc_half_shift-0.5, 0,1, color = "k", linestyle = "--")

            ax = axs[1]
            ax.set_ylabel("RMS " + var.upper())

            df_chan.groupby("channel")[var].std().plot(ax = ax, marker = "o", linestyle = "--", label = label)
            #df_chan.groupby("channel")[var].agg(iqr).plot(ax = ax, marker = "o", linestyle = "--", label = label)

        for ax in axs:
            ax.grid()
            ax.set_xlabel("channel")
            #ax.legend()

            if max_ch > roc_half_shift:
                ax.axvline(roc_half_shift-0.5, 0,1, color = "k", linestyle = "--")

        axs[0].legend()

        figname = outdir + "/" + var + "_mean_and_rms.png"
        plt.savefig(figname)

        figname = outdir + "/" + var + "_mean_and_rms.pdf"
        plt.savefig(figname)

    exit(0)
    # ## Correlations

    # In[14]:


    print("Correlation")
    var = "adc"

    chan_vals = {}

    for chan in df_chan.channel.unique():
        sel = df_chan.channel == chan
        key = chan#"C%i" %chan
        chan_vals[key] = df_chan[sel][var].values


    df_corr = pd.DataFrame(chan_vals)
    corr = df_corr.corr()
    corr[corr == 1] = 0

    f = plt.figure(figsize=(12, 10))
    plt.suptitle(title)

    zmax = corr.unstack().max()
    print "Max correlation coefficient", zmax
    im = plt.matshow(corr, fignum=f.number, cmap = "bwr", vmin=-zmax, vmax=zmax)
    cbar = plt.colorbar(im, label = "correlation")

    #cbar.clim(-zmax,zmax)

    plt.xlabel("channel")
    plt.ylabel("channel")

    plt.xticks(range(0,max_ch+1,4))
    plt.yticks(range(0,max_ch+1,4))

    if max_ch > roc_half_shift:
        plt.gca().axvline(roc_half_shift-0.5, 0,1, color = "k", linestyle = "--")
        plt.gca().axhline(roc_half_shift-0.5, 0,1, color = "k", linestyle = "--")

    #plt.show()

    figname = outdir + "/" + "adc_correlation.png"
    plt.savefig(figname)

    figname = outdir + "/" + "adc_correlation.pdf"
    plt.savefig(figname)


    # ## Plot event sequence

    # In[17]:
    f,axs = plt.subplots(9,9,figsize=(30, 20))
    plt.suptitle(title)

    axs = f.get_axes()

    print "Making dist plot"
    for chan in df_chan.channel.unique():
        ax = axs[chan]
        sel = df_chan.channel == chan
        ax.hist(df_chan[sel][var].values, 50)
        ax.legend(title = chan)

    figname = outdir + "/" + "dist_plot.pdf"
    plt.savefig(figname)
    print "dist plot done"


    '''
    print("Sequence")

    df_corr.plot(figsize=(20, 10), marker = ".", linestyle = "", cmap = "viridis")
    plt.legend(ncol = 4, fontsize = "small")

    figname = outdir + "/" + "adc_sequence.png"
    plt.savefig(figname)

    figname = outdir + "/" + "adc_sequence.pdf"
    plt.savefig(figname)
    '''


if __name__ == "__main__":


    dfs = []

    '''
    labels = ["No Cd", "Ch0 47pF", "Ch1 47pF","Ch1 47pf (2)"]

    indirs = ["/Users/artur/cernbox/www/HGCAL/electronics/HGCROCv2/measurements/first_tests/py_scripts/acq/run_20190821_155835_noCapa",
              "/Users/artur/cernbox/www/HGCAL/electronics/HGCROCv2/measurements/first_tests/py_scripts/acq/run_20190821_155938_47pF_Ch0",
              #"/Users/artur/cernbox/www/HGCAL/electronics/HGCROCv2/measurements/first_tests/py_scripts/acq/run_20190821_160154_47pF_Ch1",
              "/Users/artur/cernbox/www/HGCAL/electronics/HGCROCv2/measurements/first_tests/py_scripts/acq/run_20190821_161155_Ch1_47pF"]
    '''
    labels = ["Ped 0","Ped 15", "Ped 31"]

    indirs = ["data/acq/run_20190903_110510_ped_0/",
              "data/acq/run_20190903_112223_ped_15",
              "data/acq/run_20190903_111849_ped_31"]

    for indir in indirs:
        fname1 = indir + "/fifo4.raw"
        fname2 = indir + "/fifo5.raw"

        df = create_full_df(fname1, fname2)
        dfs.append(df)

    outdir =  indir + "/compare_plots/"
    if not os.path.exists(outdir):
        os.makedirs(outdir)
    make_plots(dfs,labels, outdir, 0)


    '''
    if len(sys.argv) == 2:
        indir = sys.argv[1]

        fname1 = indir + "/fifo4.raw"
        fname2 = indir + "/fifo5.raw"

        if not os.path.exists(fname1):
            print("Fifo4 file does not exist!")
            exit(0)
        if not os.path.exists(fname2):
            print("Fifo5 file does not exist!")
            exit(0)

        df_chan = create_full_df(fname1, fname2)

        outdir = indir + "/plots/"
        if not os.path.exists(outdir):
            os.makedirs(outdir)
        #make_plots(df_chan, outdir)
        make_plots(df_chan, outdir, 0)

    else:
        print("No argument given")
    '''
