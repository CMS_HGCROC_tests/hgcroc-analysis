import os, glob, sys
import numpy as np
import pandas as pd

calib_chid = -1
cm0_chid = -2
cm1_chid = -3

roc_half_shift = 36

event_size = 43

data_header = 0xaa
data_header_trail = 0x5

def get_event_counters(df):

    df_header = df["hex"].apply(lambda x: (int(x,16) >> 24))
    df_header_trail = df["hex"].apply(lambda x: (int(x,16) & 0x7))

    # count headers
    #is_header = df_header == data_header
    is_header = (df_header == data_header) & (df_header_trail == data_header_trail)
    #print "Found %i data headers" %np.count_nonzero(is_header)

    n_headers = np.count_nonzero(is_header)
    #if n_headers < 1:
    #    return None

    # check event size
    event_lengths = np.diff(df[is_header].index)
    if len(event_lengths) > 0:
        ev_length = event_lengths.mean()
        rms = event_lengths.std()
        #print "Mean event length %f and rms is %f" %(ev_length, rms)

        if rms >= 1:
            print("Bad event size rms! %f " %rms )
            bad_evs = np.unique(event_lengths, return_counts = True)
            bad_evs = pd.DataFrame(bad_evs).T
            bad_evs.columns = ["ev length", "# lines"]
            print(bad_evs)

    # count cumulative event number
    ev_nmbrs = is_header.cumsum()

    ### Decode header
    head_hex = df.loc[is_header,"hex"].apply(lambda x:  int(x,16))

    # BX counter
    bx_cnts = head_hex.apply( lambda x: (x >> 12) & 0xfff)
    # WADD - write address (column in DRAM)
    wadds = head_hex.apply(lambda x: (x >> 3) & 0x1ff )

    # DF with header info: BX counter, WADD
    df_header =pd.DataFrame({"event":ev_nmbrs.unique(),"bx":bx_cnts,"wadd":wadds})

    # DF with per-index event number
    df_ev_flat = pd.DataFrame({"event":ev_nmbrs})
    df_ev_flat.set_index("event", inplace = True)

    # Merge DFs to expand header info to every row
    df_ev_counters = df_ev_flat.join(df_header.set_index("event"))

    return df_ev_counters

def get_df(fname):
    #print("Reading data from %s into dataframe" %fname)

    lines = []

    with open(fname, 'r') as f:
        lines = f.read().splitlines()
        #lines = lines[1:] # remove first line
        #lines = lines[:20000]

    if len(lines) < 38:
        print("Too few lines in file!")
        #exit(0)
        print fname
        return None

    df_all = pd.DataFrame({"hex":lines})

    # reject too long lines
    sel = df_all["hex"].str.len() < 9
    df_all = df_all[sel]

    if np.count_nonzero(~sel) > 0:
        print("warning: %i lines longer than 8 chars in file:\n%s!" % (np.count_nonzero(~sel), fname))
    #exit(0)

    # get event counters and decoded header
    df_ev_counters = get_event_counters(df_all)

    df_all = df_all.join(df_ev_counters.reset_index())

    ## count index within event
    ev_idx = df_all.groupby('event').cumcount()
    df_all["idx"] = ev_idx - 1

    #df_all["event"] = df_all.index // event_size
    #df_all["idx"] = df_all.index - df_all["event"]*event_size - 1
    #df_all["channel"] = df_all.index - df_all["event"]*event_size - 1
    df_all["channel"] = df_all["idx"]# - 1

    df_all.set_index("event", inplace = True)

    ## set channel type
    df_all["ch_type"] = -1

    ### trailer
    sel = df_all.idx > 37
    df_all.loc[sel,"channel"] = -99
    df_all.loc[sel,"ch_type"] = -1
    sel = df_all.idx < 0
    df_all.loc[sel,"channel"] = -99
    df_all.loc[sel,"ch_type"] = -1

    ## channels after Calib
    sel = df_all.idx >= 0
    sel &= df_all.idx < 18
    df_all.loc[sel,"ch_type"] = 0

    ## channels after Calib
    sel = df_all.idx > 19
    sel &= df_all.idx < 38
    df_all.loc[sel,"channel"] -= 2
    df_all.loc[sel,"ch_type"] = 0

    ## CM chan
    sel = df_all.idx == 18
    df_all.loc[sel,"channel"] = -99 #"cm"
    df_all.loc[sel,"ch_type"] = 2

    ## Calib chan
    sel = df_all.idx == 19
    df_all.loc[sel,"channel"] = calib_chid #"calib"
    df_all.loc[sel,"ch_type"] = 1

    #print df_all[:50]
    #exit(0)

    return df_all


def get_chan_df(df_all):
    #print("Preparing channel df")

    sel = df_all.channel > -99
    df_chan = df_all[sel].copy()

    data = df_chan["hex"].apply(lambda x: int(x, 16))

    df_chan["adc"] = data.apply(lambda x: int(x & 0x3ff))
    df_chan["toa"] = data.apply(lambda x: int((x>>10) & 0x3ff))
    df_chan["tot"] = data.apply(lambda x: int(x>>20))

    #df_chan = df_chan[['channel', u'ch_type', u'adc', u'toa',u'tot']]
    df_chan = df_chan[["bx","wadd",'channel', u'ch_type', u'adc', u'toa',u'tot']]

    return df_chan


def get_cm_dfs(df_all):
    #print("Preparing CM df")
    # ## Decode CM channels into separate df

    sel = df_all.idx == 18

    cm0 = df_all[sel]["hex"].apply(lambda x: int((int(x, 16) >> 10) & 0x3ff))
    cm1 = df_all[sel]["hex"].apply(lambda x: int(int(x, 16) & 0x3ff))

    df_counters = df_all.loc[sel,["bx","wadd"]]

    dfs_cm = []
    for cm, cm_id in zip([cm0, cm1],[cm0_chid, cm1_chid]):
        df_cm = pd.DataFrame({"adc":cm})
        # add counters
        df_cm = df_cm.join(df_counters)
        # channel info
        df_cm["channel"] = cm_id
        df_cm["ch_type"] = 2
        df_cm["tot"] = 0
        df_cm["toa"] = 0
        dfs_cm.append(df_cm)

    return dfs_cm

def create_half_df(fname, roc_half = 0):

    df_all = get_df(fname)
    if df_all is None: return None

    df_chan = get_chan_df(df_all)
    dfs_cm = get_cm_dfs(df_all)

    df_chan = pd.concat([df_chan] + dfs_cm, sort = False)
    df_chan.sort_index(inplace = True)

    if roc_half == 1:
        # shift CM and calib channels to > +36
        sel = df_chan.ch_type != 0
        df_chan.loc[sel,"channel"] = roc_half_shift - 1 - df_chan[sel].channel
        # shift all channels to +second half
        df_chan.channel += roc_half_shift

    return df_chan

def create_full_df(fname1, fname2):

    df_chan1 = create_half_df(fname1)
    df_chan2 = create_half_df(fname2, roc_half = 1)

    if (df_chan1 is None) or (df_chan1 is None):
        return None

    df_chan = pd.concat([df_chan1,df_chan2])

    return df_chan

def process_dir(indir):

    hdfname = indir + "/dataframe.h5"

    fname1 = indir + "/fifo4.raw"
    fname2 = indir + "/fifo5.raw"

    if not os.path.exists(fname1):
        print("Fifo4 file does not exist!")
        exit(0)
    if not os.path.exists(fname2):
        print("Fifo5 file does not exist!")
        exit(0)

    df_chans = create_full_df(fname1, fname2)

    print("Saving dataframe as " + hdfname)
    df_chans.to_hdf(hdfname, key = "data", complevel = 9)

    return hdfname
