# Analysis of HGCROC characterisation data

## Package installation
In order to install the required python packages run this:
```shell
pip install --user -r requirements.txt
```

## Raw data processing
There are two scripts to process the raw data into `pandas` dataframes: 
* `process_raw.py` for DAQ data
* `process_trg_raw.py` for trigger

Both scripts have to be run with the `run_X` folder (or a subfolder) as argument. 
Further options are visible with the `-h` help.

In both cases the script will pick up variables from the folder substructure (following the `run_X` folder). 
The variables are added as columns to the dataframe.

### DAQ data 
Only the data folder path is an argument now.

```bash
$ python process_raw.py -h                                                                                                                                  [10:56:22]
usage: process_raw.py [-h] input_dir

positional arguments:
  input_dir   Input directory

optional arguments:
  -h, --help  show this help message and exit
```

### Trigger data
In addition to the data path, the `sumTC9` key is an option. By default 4-channel TC sums are decoded, otherwise 9-channel sums when the `--sumTC9` argument is used.
```bash
$ python process_trg_raw.py -h                                                                                                                              [10:56:26]
usage: process_trg_raw.py [-h] [--sumTC9] input_dir

positional arguments:
  input_dir   Input directory

optional arguments:
  -h, --help  show this help message and exit
  --sumTC9    Use 9 cell TC sums
```
