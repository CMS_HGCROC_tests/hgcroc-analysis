from converter import *

import matplotlib as mpl
mpl.use('agg')
import matplotlib.pyplot as plt

from scipy.stats import iqr

font = {'size'   : 18}
mpl.rc('font', **font)

def make_plots(df_chan, outdir = "./", plot_style = 0):
    print("Making plots into dir: " + outdir)
    # ## Plot
    title = outdir.split("/")
    title = title[-4]

    max_ch = df_chan.channel.max()

    print("Mean and rms")
    # ## Mean and RMS of ADC, TOT, TOA
    for var in ["adc", "tot","toa"]:

        #plt.figure(figsize = (16,8))
        if plot_style == 0:
            f, axs = plt.subplots(2,1,figsize = (16,8), sharex = True, gridspec_kw={'height_ratios': [2, 1]})
            plt.subplots_adjust(wspace=0, hspace=0)
        else:
            f, axs = plt.subplots(1,2,figsize = (16,8))

        #plt.subplot(121)
        ax = axs[0]
        #ax.set_title(title)
        plt.suptitle(title)

        sel = df_chan.ch_type == 0
        df_chan[sel].groupby("channel")[var].mean().plot(ax = ax, marker = "o", linestyle = "--", label = "channels")

        sel = df_chan.ch_type == 1
        df_chan[sel].groupby("channel")[var].mean().plot(ax = ax, marker = "o", linestyle = "", label = "calib")

        if var == "adc":
            sel = df_chan.ch_type == 2
            df_chan[sel].groupby("channel")[var].mean().plot(ax = ax, marker = "o", linestyle = "", label = "cm")

        ax.set_xlabel("channel")
        ax.set_ylabel("Mean " + var.upper())
        ax.legend()
        ax.grid()

        if max_ch > roc_half_shift:
            ax.axvline(roc_half_shift-0.5, 0,1, color = "k", linestyle = "--")

        #plt.show()

        #plt.subplot(122)
        ax = axs[1]

        #sel = df_chan.channel < 37
        sel = df_chan.ch_type == 0
        df_chan[sel].groupby("channel")[var].std().plot(ax = ax, marker = "o", linestyle = "--", label = "channels")
        #df_chan[sel].groupby("channel")[var].agg(iqr).plot(ax = ax, marker = "o", linestyle = "--", label = "channels")

        sel = df_chan.ch_type == 1
        df_chan[sel].groupby("channel")[var].std().plot(ax = ax, marker = "o", linestyle = "", label = "calib")

        if var == "adc":
            sel = df_chan.ch_type == 2
            df_chan[sel].groupby("channel")[var].std().plot(ax = ax, marker = "o", linestyle = "", label = "cm")

        ax.set_xlabel("channel")
        ax.set_ylabel("RMS " + var.upper())
        #ax.legend()
        ax.grid()

        if max_ch > roc_half_shift:
            ax.axvline(roc_half_shift-0.5, 0,1, color = "k", linestyle = "--")

        figname = outdir + "/" + var + "_mean_and_rms.png"
        plt.savefig(figname)

        figname = outdir + "/" + var + "_mean_and_rms.pdf"
        plt.savefig(figname)

    # ## Correlations

    # In[14]:


    print("Correlation")
    var = "adc"

    chan_vals = {}

    chans = sorted(df_chan.channel.unique())
    for chan in chans:
        sel = df_chan.channel == chan
        key = chan#"C%i" %chan
        chan_vals[key] = df_chan[sel][var].values


    df_corr = pd.DataFrame(chan_vals)
    corr = df_corr.corr()
    corr[corr == 1] = 0

    f = plt.figure(figsize=(12, 10))
    plt.suptitle(title)

    zmax = corr.unstack().max()
    print "Max correlation coefficient", zmax
    im = plt.matshow(corr, fignum=f.number, cmap = "bwr", vmin=-zmax, vmax=zmax)
    cbar = plt.colorbar(im, label = "correlation")

    #cbar.clim(-zmax,zmax)

    plt.xlabel("channel")
    plt.ylabel("channel")

    plt.xticks(chans[::4] - chans[0],chans[::4])
    plt.yticks(chans[::4] - chans[0],chans[::4])

    #plt.xticks(range(0,max_ch+1,4))
    #plt.yticks(range(0,max_ch+1,4))

    if max_ch > roc_half_shift:
        plt.gca().axvline(roc_half_shift-0.5 + 3, 0,1, color = "k", linestyle = "--")
        plt.gca().axhline(roc_half_shift-0.5 + 3, 0,1, color = "k", linestyle = "--")

    #plt.show()

    figname = outdir + "/" + "adc_correlation.png"
    plt.savefig(figname)

    figname = outdir + "/" + "adc_correlation.pdf"
    plt.savefig(figname)

    ### 1D hist of correlations
    f = plt.figure(figsize=(12, 10))
    plt.suptitle(title)

    #print corr.values

    vals = np.triu(corr)
    #print vals

    sel = ~np.isnan(vals)
    sel &= vals != 0

    vals = vals[sel]

    r = plt.hist(vals, 30)
    #print corr
    #print corr.shape

    figname = outdir + "/" + "adc_correlation_hist.png"
    plt.savefig(figname)
    figname = outdir + "/" + "adc_correlation_hist.pdf"
    plt.savefig(figname)


    # ## Plot event sequence

    # In[17]:
    f,axs = plt.subplots(9,9,figsize=(30, 20))
    plt.suptitle(title)

    axs = f.get_axes()

    print "Making dist plot"
    for chan in df_chan.channel.unique():
        ax = axs[chan]
        sel = df_chan.channel == chan
        ax.hist(df_chan[sel][var].values, 50)

    figname = outdir + "/" + "dist_plot.pdf"
    plt.savefig(figname)
    print "dist plot done"


    '''
    print("Sequence")

    df_corr.plot(figsize=(20, 10), marker = ".", linestyle = "", cmap = "viridis")
    plt.legend(ncol = 4, fontsize = "small")

    figname = outdir + "/" + "adc_sequence.png"
    plt.savefig(figname)

    figname = outdir + "/" + "adc_sequence.pdf"
    plt.savefig(figname)
    '''


if __name__ == "__main__":

    if len(sys.argv) == 2:
        indir = sys.argv[1]

        fname1 = indir + "/fifo4.raw"
        fname2 = indir + "/fifo5.raw"

        if not os.path.exists(fname1):
            print("Fifo4 file does not exist!")
            exit(0)
        if not os.path.exists(fname2):
            print("Fifo5 file does not exist!")
            exit(0)

        df_chan = create_full_df(fname1, fname2)
        print("Max event: %i" % df_chan.index.max() )

        outdir = indir + "/plots/"
        if not os.path.exists(outdir):
            os.makedirs(outdir)
        #make_plots(df_chan, outdir)
        make_plots(df_chan, outdir, 0)

    else:
        print("No argument given")
