from converter import *

def main(indir):

    fnames = glob.glob(indir + "/*/fifo4.raw")
    print("Found %i files" %len(fnames))

    dfs = []

    cnt = 0
    for i, fname in enumerate(sorted(fnames)):

        if i%10==0: print i,

        #print fname
        if os.path.getsize(fname) < 100:
            print os.path.getsize(fname)
            continue

        parts = fname.split("/")

        ped_val = int(parts[-2].split("_")[-1])

        print "Parts:", ped_val

        cnt+=1

        fname1 = fname
        fname2 = fname.replace("fifo4","fifo5")

        df_chans = create_full_df(fname1, fname2)
        if df_chans is None: continue

        df_chans["ped_DAC"] = ped_val

        dfs.append(df_chans)

        #print calib_dac

    print("Used %i files" % cnt)

    # merger dfs
    df_chans = pd.concat(dfs)
    df_chans.sort_values("ped_DAC", inplace=True)

    hdfname = indir + "/dataframe.h5"
    df_chans.to_hdf(hdfname, key = "data")

    return

if __name__ == "__main__":

    if len(sys.argv) == 2:
        indir = sys.argv[1]

        main(indir)

    else:
        print("No argument given")
